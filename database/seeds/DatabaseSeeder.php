<?php

use Illuminate\Database\Seeder;
use App\Models\Todo;
use Faker\Factory;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $faker = Factory::create();

        $todo = new Todo();
        $todo->name = $faker->name;
        $todo->content = $faker->paragraph;
        $todo->save();

    }
}
