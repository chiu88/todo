<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Faker\Factory;
use App\Models\Todo;

Route::get('/', function () {

    $faker = Factory::create();

    $todos = Todo::all();

    $data = [
        'todos' => $todos,
    ];


    return view('welcome', $data);
});
